package com.annysaeka.listviewsql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Annysa Eka on 3/23/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "mahasiswa.db";
    public static final String TABLE_NAME = "tb_mahasiswa";
    public static final String COL_1 = "NRP";
    public static final String COL_2 = "NAMA";
    public static final String COL_3 = "JURUSAN";
    public static final String COL_4 = "ANGKATAN";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_NAME +" (NRP INTEGER PRIMARY KEY,NAMA TEXT,JURUSAN TEXT,ANGKATAN INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }


    public Cursor getAllData() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+TABLE_NAME,null);
        return res;
    }


    public boolean insertData(String nrp,String nama,String jurusan, String angkatan) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_1, nrp);
        contentValues.put(COL_2,nama);
        contentValues.put(COL_3,jurusan);
        contentValues.put(COL_4,angkatan);
        long result = db.insert(TABLE_NAME,null ,contentValues);
        if(result == -1)
            return false;
        else
            return true;
    }
}
