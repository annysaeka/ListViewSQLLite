package com.annysaeka.listviewsql;

import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ViewData extends AppCompatActivity {

    DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_data);
        this.setTitle( "TUGAS 6 - SAB");

        ListView listView = (ListView) findViewById(R.id.listView);
        myDB = new DatabaseHelper(this);

        ArrayList<String> theList = new ArrayList<>();
        Cursor data = myDB.getAllData();
        if(data.getCount() == 0){
            Toast.makeText(this, "Data Mahasiswa Tidak Ada!",Toast.LENGTH_LONG).show();
        }else{
            while(data.moveToNext()){
                theList.add("NRP : " + data.getString(0) + "\n" + "Nama : " + data.getString(1) + "\n" +
                        "Jurusan : " + data.getString(2) + "\n" + "Angkatan : " + data.getString(3));
                ListAdapter listAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,theList);
                listView.setAdapter(listAdapter);
            }
        }
    }
}
