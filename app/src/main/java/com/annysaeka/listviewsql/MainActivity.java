package com.annysaeka.listviewsql;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    DatabaseHelper myDb;
    EditText editNama,editNrp,editJurusan ,editAngkatan;
    Button btnAddData;
    Button btnviewAll;
    //Button btnviewAll2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setTitle( "TUGAS 6 - SAB");

        myDb = new DatabaseHelper(this);
        editNrp = (EditText) findViewById(R.id.editText_nrp);
        editNama = (EditText) findViewById(R.id.editText_nama);
        editAngkatan = (EditText) findViewById(R.id.editText_angkatan);
        editJurusan = (EditText) findViewById(R.id.editText_jurusan);
        btnAddData = (Button) findViewById(R.id.button_add);
        btnviewAll = (Button)findViewById(R.id.button_viewAll);
       //btnviewAll2 = (Button)findViewById(R.id.button_viewAll2);
        AddData();
        //viewAll();

        btnviewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ViewData.class);
                startActivity(intent);
            }
        });
    }

    public  void AddData() {
        btnAddData.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean isInserted = myDb.insertData(editNrp.getText().toString(),
                                editNama.getText().toString(),
                                editJurusan.getText().toString(), editAngkatan.getText().toString() );
                        if(isInserted == true)
                            Toast.makeText(MainActivity.this,"Data Berhasil Ditambahkan !",Toast.LENGTH_LONG).show();
                        else
                            Toast.makeText(MainActivity.this,"Data Gagal Ditambahkan !",Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

//    public void viewAll() {
//        btnviewAll2.setOnClickListener(
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Cursor res = myDb.getAllData();
//                        if(res.getCount() == 0) {
//                            // show message
//                            showMessage("Error","Nothing found");
//                            return;
//                        }
//
//                        StringBuffer buffer = new StringBuffer();
//                        while (res.moveToNext()) {
//                            buffer.append("NRP : "+ res.getString(0)+"\n");
//                            buffer.append("Nama : "+ res.getString(1)+"\n");
//                            buffer.append("Jurusan : "+ res.getString(2)+"\n");
//                            buffer.append("Angkatan : "+ res.getString(3)+"\n\n");
//                        }
//
//                        // Show all data
//                        showMessage("Data",buffer.toString());
//                    }
//                }
//        );
//    }

    public void showMessage(String title,String Message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }
}
